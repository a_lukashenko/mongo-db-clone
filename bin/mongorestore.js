#! /usr/bin/env node
// tslint:disable:no-console

const shell = require('shelljs')
const restore = require('../lib/restore.js')

console.log('start mongorestore')
restore(process.argv)
  .then(() => {
    console.log('restore database done')
  })
  .catch(err => {
    console.error(err)
    shell.exit(1)
  })
