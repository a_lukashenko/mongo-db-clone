#! /usr/bin/env node
// tslint:disable:no-console

const shell = require('shelljs')
const argv = require('yargs').argv
const copy = require('../lib/copy.js')

console.log('start mongodb-copy')
copy(argv.s, argv.d)
  .then(() => {
    console.log('copy database done')
  })
  .catch(err => {
    console.error(err)
    shell.exit(1)
  })
