#! /usr/bin/env node
// tslint:disable:no-console

const shell = require('shelljs')
const dump = require('../lib/dump.js')

console.log('start mongodump')
dump(process.argv)
  .then(() => {
    console.log('dump database done')
  })
  .catch(err => {
    console.error(err)
    shell.exit(1)
  })
