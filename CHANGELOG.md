# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [3.1.0](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.13...@tresmo/mongodb-copy@3.1.0) (2020-09-04)


### Features

* allow mongodb srv uri ([6cab398](https://github.com/tresmo/TREMOD/commit/6cab398d29f0fcd73684fce5548d24745f535e8f))





## [3.0.13](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.12...@tresmo/mongodb-copy@3.0.13) (2020-05-18)

**Note:** Version bump only for package @tresmo/mongodb-copy





## [3.0.12](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.11...@tresmo/mongodb-copy@3.0.12) (2020-03-09)

**Note:** Version bump only for package @tresmo/mongodb-copy





## [3.0.11](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.10...@tresmo/mongodb-copy@3.0.11) (2020-02-04)


### Bug Fixes

* **deps:** update dependency getos to v3.1.4 ([#677](https://github.com/tresmo/TREMOD/issues/677)) ([47b4acc](https://github.com/tresmo/TREMOD/commit/47b4acc96a510cdbe1c296aa0f4537345a5ecfbb))
* **deps:** update dependency request-promise to v4.2.5 ([#637](https://github.com/tresmo/TREMOD/issues/637)) ([3572a9f](https://github.com/tresmo/TREMOD/commit/3572a9f8f47d7fdbbf14e09f769f695e3e16854c))
* **deps:** update dependency rimraf to v3.0.1 ([#679](https://github.com/tresmo/TREMOD/issues/679)) ([d7da43a](https://github.com/tresmo/TREMOD/commit/d7da43a50159fa89e431e20d7f4aa799103c913e))
* **deps:** update dependency semver to v7 ([1a35ecf](https://github.com/tresmo/TREMOD/commit/1a35ecf717f96e77705e957183eb480a75bf627c))
* **deps:** update dependency uuid to v3.4.0 ([#674](https://github.com/tresmo/TREMOD/issues/674)) ([fa2c7df](https://github.com/tresmo/TREMOD/commit/fa2c7df3a42ab5e961ec9e354c6d19346bce6fca))
* **deps:** update dependency yargs to v14.2.0 ([#622](https://github.com/tresmo/TREMOD/issues/622)) ([aaa4cb4](https://github.com/tresmo/TREMOD/commit/aaa4cb46b740ef7f53e60a2e948ca6842b1b8ab1))
* **deps:** update dependency yargs to v14.2.1 ([#646](https://github.com/tresmo/TREMOD/issues/646)) ([c5727d7](https://github.com/tresmo/TREMOD/commit/c5727d7851fe0593614020b44ab6ba6d2f30ac7c))
* **deps:** update dependency yargs to v14.2.2 ([#649](https://github.com/tresmo/TREMOD/issues/649)) ([79ae7f9](https://github.com/tresmo/TREMOD/commit/79ae7f94bbfdeb75a5af5aa66a24790a3129de59))
* **deps:** update dependency yargs to v15 ([6c11e8a](https://github.com/tresmo/TREMOD/commit/6c11e8a71af7c61c5f6f2137c06c11c37d4f7741))
* skip folder creating if already created ([f273b3d](https://github.com/tresmo/TREMOD/commit/f273b3d1da40837b25e9609d0039d6c44a845441))





## [3.0.10](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.9...@tresmo/mongodb-copy@3.0.10) (2019-09-16)


### Bug Fixes

* **deps:** update dependency rimraf to v3 ([1aee0c7](https://github.com/tresmo/TREMOD/commit/1aee0c7))
* **deps:** update dependency uuid to v3.3.3 ([c0c9c79](https://github.com/tresmo/TREMOD/commit/c0c9c79))
* correctly parse int ([8498e6d](https://github.com/tresmo/TREMOD/commit/8498e6d))
* correctly parse major debian version and download correct bins ([d25ee39](https://github.com/tresmo/TREMOD/commit/d25ee39))
* **deps:** update dependency yargs to v14 ([6320bd5](https://github.com/tresmo/TREMOD/commit/6320bd5))
* **mongodb-copy:** update used mongodb version ([3a6dc73](https://github.com/tresmo/TREMOD/commit/3a6dc73))





<a name="3.0.9"></a>
## [3.0.9](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.8...@tresmo/mongodb-copy@3.0.9) (2019-05-29)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="3.0.8"></a>
## [3.0.8](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.7...@tresmo/mongodb-copy@3.0.8) (2019-05-27)


### Bug Fixes

* **deps:** update dependency yargs to v13 ([b5fcb4d](https://github.com/tresmo/TREMOD/commit/b5fcb4d))
* **deps:** update dependency yargs to v13.2.4 ([d162aa6](https://github.com/tresmo/TREMOD/commit/d162aa6))




<a name="3.0.7"></a>
## [3.0.7](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.6...@tresmo/mongodb-copy@3.0.7) (2019-03-06)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="3.0.6"></a>
## [3.0.6](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.5...@tresmo/mongodb-copy@3.0.6) (2019-01-28)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="3.0.5"></a>
## [3.0.5](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.4...@tresmo/mongodb-copy@3.0.5) (2018-12-20)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="3.0.4"></a>
## [3.0.4](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.3...@tresmo/mongodb-copy@3.0.4) (2018-12-12)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="3.0.3"></a>
## [3.0.3](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.2...@tresmo/mongodb-copy@3.0.3) (2018-12-07)


### Bug Fixes

* use mongodb version 4 to be compatible to all used os ([0e026b4](https://github.com/tresmo/TREMOD/commit/0e026b4))




<a name="3.0.2"></a>
## [3.0.2](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.1...@tresmo/mongodb-copy@3.0.2) (2018-12-06)


### Bug Fixes

* fix path to main command ([b764bcf](https://github.com/tresmo/TREMOD/commit/b764bcf))




<a name="3.0.1"></a>
## [3.0.1](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@3.0.0...@tresmo/mongodb-copy@3.0.1) (2018-12-06)


### Bug Fixes

* restore bin commands function with new wrappers ([49c4dc4](https://github.com/tresmo/TREMOD/commit/49c4dc4))




<a name="3.0.0"></a>
# [3.0.0](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@2.2.1...@tresmo/mongodb-copy@3.0.0) (2018-12-05)


### Bug Fixes

* **deps:** update dependency shelljs to v0.8.3 ([d1679e0](https://github.com/tresmo/TREMOD/commit/d1679e0))
* **deps:** update dependency yargs to v12.0.4 ([4afce7f](https://github.com/tresmo/TREMOD/commit/4afce7f))
* **deps:** update dependency yargs to v12.0.5 ([6893289](https://github.com/tresmo/TREMOD/commit/6893289))


### Features

* use new download logic for up to date mongodb version ([225d9e6](https://github.com/tresmo/TREMOD/commit/225d9e6))


### BREAKING CHANGES

* use newer mongodb version




<a name="2.2.1"></a>
## [2.2.1](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@2.2.0...@tresmo/mongodb-copy@2.2.1) (2018-10-10)


### Bug Fixes

* **deps:** update dependency yargs to v12 ([#238](https://github.com/tresmo/TREMOD/issues/238)) ([3fcd9de](https://github.com/tresmo/TREMOD/commit/3fcd9de))
* **deps:** update dependency yargs to v12.0.2 ([88245ba](https://github.com/tresmo/TREMOD/commit/88245ba))




<a name="2.2.0"></a>
# [2.2.0](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@2.1.0...@tresmo/mongodb-copy@2.2.0) (2018-07-13)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="2.1.0"></a>
# [2.1.0](https://github.com/tresmo/TREMOD/compare/@tresmo/mongodb-copy@2.1.0...@tresmo/mongodb-copy@2.1.0) (2018-07-13)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="2.0.1"></a>
## 2.0.1 (2018-04-20)




**Note:** Version bump only for package @tresmo/mongodb-copy

<a name="1.3.2"></a>

## [1.3.2](https://github.com/tresmo/mongodb-copy/compare/v1.3.1...v1.3.2) (2018-04-19)

<a name="1.3.1"></a>

## [1.3.1](https://github.com/tresmo/mongodb-copy/compare/v1.3.0...v1.3.1) (2018-03-06)

### Features

* fix postinstall not working with lerna monorepo ([b9223b9](https://github.com/tresmo/mongodb-copy/commit/b9223b9))

<a name="1.3.0"></a>

# [1.3.0](https://github.com/tresmo/mongodb-copy/compare/v1.2.0...v1.3.0) (2018-03-02)

### Features

* search bin dir in parent folders (lerna hoist mode compatibility) ([4a6615d](https://github.com/tresmo/mongodb-copy/commit/4a6615d))

<a name="1.2.0"></a>

# [1.2.0](https://github.com/tresmo/mongodb-copy/compare/v1.1.4...v1.2.0) (2018-01-22)

### Bug Fixes

* **copy-db:** delete directory with files if created ([f93d3dc](https://github.com/tresmo/mongodb-copy/commit/f93d3dc))

<a name="1.1.4"></a>

## [1.1.4](https://github.com/tresmo/mongodb-copy/compare/v1.1.3...v1.1.4) (2016-08-31)

### Bug Fixes

* **copy-db:** return error message if no data is dumped from database ([0572e34](https://github.com/tresmo/mongodb-copy/commit/0572e34))
