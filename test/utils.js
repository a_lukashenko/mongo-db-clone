'use strict'

const chai = require('chai')
const assert = chai.assert
const utils = require('../lib/utils.js')

describe('test utils functions', () => {
  let binPath

  before(function (done) {
    binPath = utils.getBinDir()
    done()
  })

  describe('checkPrerequisites()', () => {
    it('Should return Promise.reject if sourceDbUrl is missing', (done) => {
      utils
        .checkPrerequisites()
        .then(() => {
          done('should not end up here')
        })
        .catch(() => {
          done()
        })
    })

    it('Should return Promise.reject if sourceDbUrl is not an mongodb url', (done) => {
      utils
        .checkPrerequisites('xxx')
        .then(() => {
          done('should not end up here')
        })
        .catch(() => {
          done()
        })
    })

    it('Should return Promise.reject if destinationDbUrl is missing', (done) => {
      utils
        .checkPrerequisites('mongodb://localhost/cp2')
        .then(() => {
          done('should not end up here')
        })
        .catch(() => {
          done()
        })
    })

    it('Should return Promise.reject if destinationDbUrl is not an mongodb url', (done) => {
      utils
        .checkPrerequisites('mongodb://localhost/cp2', 'xxx')
        .then(() => {
          done('should not end up here')
        })
        .catch(() => {
          done()
        })
    })

    it('Should return Promise.resolve if every check is OK (case 1)', (done) => {
      utils
        .checkPrerequisites('mongodb://localhost/cp1', 'mongodb://localhost/cp2')
        .then(() => {
          done()
        })
        .catch(() => {
          done('should not end up here')
        })
    })

    it('Should return Promise.resolve if every check is OK (case 2)', (done) => {
      utils
        .checkPrerequisites('mongodb+srv://localhost/cp1', 'mongodb+srv://localhost/cp2')
        .then(() => {
          done()
        })
        .catch(() => {
          done('should not end up here')
        })
    })
  })

  describe('createDumpCmd()', () => {
    it('Should return valid string without options', () => {
      const cmd = utils.createDumpCmd('mongodb://localhost/cp', 'tmp')

      assert.equal(cmd, binPath + '/mongodump --uri mongodb://localhost/cp -o tmp')
    })

    it('Should return valid string with include collection', () => {
      const cmd = utils.createDumpCmd('mongodb://localhost/cp', 'tmp', 'partner')

      assert.equal(cmd, binPath + '/mongodump --uri mongodb://localhost/cp -o tmp -c partner')
    })

    it('Should return valid string with single exclude collection', () => {
      const cmd = utils.createDumpCmd('mongodb://localhost/cp', 'tmp', null, 'partner')

      assert.equal(
        cmd,
        binPath + '/mongodump --uri mongodb://localhost/cp -o tmp --excludeCollection partner'
      )
    })

    it('Should return valid string with exclude collections', () => {
      const cmd = utils.createDumpCmd('mongodb://localhost/cp', 'tmp', null, [
        'partner',
        'customer',
      ])

      assert.equal(
        cmd,
        binPath +
          '/mongodump --uri mongodb://localhost/cp -o tmp --excludeCollection partner --excludeCollection customer'
      )
    })

    it('Should return valid string ignoring excluding collections if including and excluding collections are given', () => {
      const cmd = utils.createDumpCmd('mongodb://localhost/cp', 'tmp', 'partner', 'customer')

      assert.equal(cmd, binPath + '/mongodump --uri mongodb://localhost/cp -o tmp -c partner')
    })
  })

  describe('getDatabaseDir()', () => {
    it('Should return valid string', (done) => {
      const dir = utils.getDatabaseDir('tmp', 'mongodb://localhost/cp1')
      assert.equal(dir, 'tmp/cp1')
      done()
    })
  })

  describe('createRestoreCmd()', () => {
    it('Should return valid string', () => {
      const cmd = utils.createRestoreCmd(
        'mongodb://localhost/cp1',
        'mongodb://localhost/cp2',
        'tmp'
      )

      assert.equal(
        cmd,
        binPath + '/mongorestore --uri mongodb://localhost/cp2 --drop --db cp2 --dir tmp/cp1'
      )
    })
  })
})
