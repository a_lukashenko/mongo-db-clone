'use strict'

// tslint:disable:no-console

const path = require('path')
const rimraf = require('rimraf')
const chai = require('chai')
const assert = chai.assert
const mongoose = require('mongoose')
const dump = require('../lib/dump.js')
const restore = require('../lib/restore.js')
const uuid = require('uuid')

const testColl1 = 'testColl1'
const testColl2 = 'testColl2'
const testColl3 = 'testColl3'
const testColl4 = 'testColl4'

describe('test dump database functions:', () => {
  const dbPrefix = 'mongodb://localhost/'
  const dbName = uuid.v4()
  const dbSource = dbPrefix + dbName
  const dbOptions = { useNewUrlParser: true }

  before(() => {
    // create database
    return createDatabase(dbSource, dbOptions)
      .then(() => dump(['', '', '-h localhost:27017', `-d ${dbName}`, `-o ${dbName}`]))
  })

  after(() => {
    // delete databases
    return deleteDatabase(dbSource, dbOptions)
      .then(() => deleteOutputFolder(dbName))
  })

  describe('test dump of whole database', () => {

    const dbNameDest = uuid.v4()
    const dbDest = dbPrefix + dbNameDest

    after(() => {
      // delete databases
      return deleteDatabase(dbDest, dbOptions)
    })

    it('restore whole database', function() {
      this.timeout(20000)

      return restore(['', '', '-h localhost:27017', `-d ${dbNameDest}`, '--drop', `--dir ${path.resolve(__dirname, '..', dbName, dbName)}`])
        .then(() => {
          return mongoose.connect(dbDest, dbOptions)
        })
        .then(() => {
          const db = mongoose.connection.db
          return db.collection(testColl1).countDocuments()
            .then(count => assert.equal(3, count))
        })
        .then(() => {
          const db = mongoose.connection.db
          return db.collection(testColl2).countDocuments()
            .then(count => assert.equal(4, count))
        })
        .then(() => {
          const db = mongoose.connection.db
          return db.collection(testColl3).countDocuments()
            .then(count => assert.equal(2, count))
        })
        .then(() => {
          const db = mongoose.connection.db
          return db.collection(testColl4).countDocuments()
            .then(count => assert.equal(4, count))
        })
        .then(() => mongoose.connection.close())
        .catch(() => mongoose.connection.close())
    })
  })
})

function createDatabase(database, options) {
  return mongoose.connect(database, options)
    .then(() => {
      return Promise.all([
        mongoose.connection.db.collection(testColl1).insertMany([{ x: 1 }, { x: 2 }, { x: 3 }]),
        mongoose.connection.db.collection(testColl2).insertMany([{ y: 1 }, { y: 2 }, { y: 3 }, { y: 4 }]),
        mongoose.connection.db.collection(testColl3).insertMany([{ z: 1 }, { z: 4 }]),
        mongoose.connection.db.collection(testColl4).insertMany([{ p: 1 }, { p: 4 }, { p: 9 }, { p: 16 }])
      ])
    })
    .then(() => {
      return mongoose.connection.close()
    })
    .catch(err => console.error(err))
}

function deleteDatabase(database, options) {
  return mongoose.connect(database, options)
    .then(() => {
      return mongoose.connection.db.dropDatabase()
    })
    .then(() => {
      return mongoose.connection.close()
    })
    .catch(err => console.error(err))
}

function deleteOutputFolder(folder) {
  return new Promise((resolve, reject) => {
    rimraf(path.resolve(__dirname, '..', folder), err => {
      if (err) {
        reject(err)
      }
      resolve()
    })
  })
}
