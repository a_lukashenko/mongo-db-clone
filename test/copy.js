'use strict'

// tslint:disable:no-console

const chai = require('chai')
const assert = chai.assert
const mongoose = require('mongoose')
const copy = require('../lib/copy.js')
const uuid = require('uuid')

const testColl1 = 'testColl1'
const testColl2 = 'testColl2'
const testColl3 = 'testColl3'
const testColl4 = 'testColl4'

describe('test copy database functions:', () => {
  const dbPrefix = 'mongodb://localhost/'
  const dbSource = dbPrefix + uuid.v4()
  const dbOptions = { useNewUrlParser: true, useUnifiedTopology: true }

  before(() => {
    // create database
    return createDatabase(dbSource, dbOptions)
  })

  after(() => {
    // delete databases
    return deleteDatabase(dbSource, dbOptions)
  })

  describe('test of whole database as callback:', () => {
    const dbDest = dbPrefix + uuid.v4()

    after(() => {
      // delete databases
      return deleteDatabase(dbDest, dbOptions)
    })

    it('copy whole database callback', function (done) {
      this.timeout(20000)
      copy(dbSource, dbDest, (err) => {
        if (err) {
          return done(err)
        }
        mongoose.connect(dbDest, dbOptions, (err1) => {
          if (err1) {
            return done(err1)
          }
          const db = mongoose.connection.db
          db.collection(testColl1).countDocuments((err2, count1) => {
            if (err2) {
              return done(err2)
            }
            assert.equal(4, count1)
            db.collection(testColl2).countDocuments((err3, count2) => {
              if (err3) {
                return done(err3)
              }
              assert.equal(4, count2)
              db.collection(testColl3).countDocuments((err4, count3) => {
                if (err4) {
                  return done(err4)
                }
                assert.equal(4, count3)
                db.collection(testColl4).countDocuments((err5, count4) => {
                  if (err5) {
                    return done(err5)
                  }
                  assert.equal(4, count4)
                  mongoose.connection.close((err6) => done(err6))
                })
              })
            })
          })
        })
      })
    })

    it('copy failed if db destination is no mongodb-URL', (done) => {
      copy(dbSource, 'thisIsNoMongoDBUrl', (err) => {
        if (err) {
          done()
        } else {
          done('should not end up here')
        }
      })
    })
  })

  describe('test with given collection to copy as callback:', () => {
    const dbDest = dbPrefix + uuid.v4()

    after(() => {
      // delete databases
      return deleteDatabase(dbDest, dbOptions)
    })

    it('copy database collection callback', function (done) {
      this.timeout(20000)
      copy(dbSource, dbDest, testColl2, (err) => {
        if (err) {
          return done(err)
        }
        mongoose.connect(dbDest, dbOptions, (err1) => {
          if (err1) {
            return done(err1)
          }
          const db = mongoose.connection.db
          db.collection(testColl1).countDocuments((err2, count1) => {
            if (err2) {
              return done(err2)
            }
            assert.equal(0, count1)
            db.collection(testColl2).countDocuments((err3, count2) => {
              if (err3) {
                return done(err3)
              }
              assert.equal(4, count2)
              db.collection(testColl3).countDocuments((err4, count3) => {
                if (err4) {
                  return done(err4)
                }
                assert.equal(0, count3)
                db.collection(testColl4).countDocuments((err5, count4) => {
                  if (err5) {
                    return done(err5)
                  }
                  assert.equal(0, count4)
                  mongoose.connection.close((err6) => done(err6))
                })
              })
            })
          })
        })
      })
    })
  })

  describe('test with excluded collections to copy as callback:', () => {
    const dbDest = dbPrefix + uuid.v4()

    after(() => {
      // delete databases
      return deleteDatabase(dbDest, dbOptions)
    })

    it('copy database excluded collections callback', function (done) {
      this.timeout(20000)
      copy(dbSource, dbDest, null, [testColl2, testColl3], (err) => {
        if (err) {
          return done(err)
        }
        mongoose.connect(dbDest, dbOptions, (err1) => {
          if (err1) {
            return done(err1)
          }
          const db = mongoose.connection.db
          db.collection(testColl1).countDocuments((err2, count1) => {
            if (err2) {
              return done(err2)
            }
            assert.equal(4, count1)
            db.collection(testColl2).countDocuments((err3, count2) => {
              if (err3) {
                return done(err3)
              }
              assert.equal(0, count2)
              db.collection(testColl3).countDocuments((err4, count3) => {
                if (err4) {
                  return done(err4)
                }
                assert.equal(0, count3)
                db.collection(testColl4).countDocuments((err5, count4) => {
                  if (err5) {
                    return done(err5)
                  }
                  assert.equal(4, count4)
                  mongoose.connection.close((err6) => done(err6))
                })
              })
            })
          })
        })
      })
    })
  })

  describe('test of whole database as promise:', () => {
    const dbDest = dbPrefix + uuid.v4()

    after(() => {
      // delete databases
      return deleteDatabase(dbDest, dbOptions)
    })

    it('copy whole database promise', function (done) {
      this.timeout(20000)
      copy(dbSource, dbDest)
        .then(() => {
          mongoose.connect(dbDest, dbOptions, (err1) => {
            if (err1) {
              return done(err1)
            }
            const db = mongoose.connection.db
            db.collection(testColl1).countDocuments((err2, count1) => {
              if (err2) {
                return done(err2)
              }
              assert.equal(4, count1)
              db.collection(testColl2).countDocuments((err3, count2) => {
                if (err3) {
                  return done(err3)
                }
                assert.equal(4, count2)
                db.collection(testColl3).countDocuments((err4, count3) => {
                  if (err4) {
                    return done(err4)
                  }
                  assert.equal(4, count3)
                  db.collection(testColl4).countDocuments((err5, count4) => {
                    if (err5) {
                      return done(err5)
                    }
                    assert.equal(4, count4)
                    mongoose.connection.close((err6) => done(err6))
                  })
                })
              })
            })
          })
        })
        .catch((err) => {
          done(err)
        })
    })

    it('copy failed if db destination is no mongodb-URL', (done) => {
      copy(dbSource, uuid.v4())
        .then(() => {
          done('should not end up here')
        })
        .catch(() => {
          done()
        })
    })
  })

  describe('test with given collection to copy as promise:', () => {
    const dbDest = dbPrefix + uuid.v4()

    after(() => {
      // delete databases
      return deleteDatabase(dbDest, dbOptions)
    })

    it('copy database collection promise', function (done) {
      this.timeout(20000)
      copy(dbSource, dbDest, testColl2)
        .then(function () {
          mongoose.connect(dbDest, dbOptions, (err1) => {
            if (err1) {
              return done(err1)
            }
            const db = mongoose.connection.db
            db.collection(testColl1).countDocuments((err2, count1) => {
              if (err2) {
                return done(err2)
              }
              assert.equal(0, count1)
              db.collection(testColl2).countDocuments((err3, count2) => {
                if (err3) {
                  return done(err3)
                }
                assert.equal(4, count2)
                db.collection(testColl3).countDocuments((err4, count3) => {
                  if (err4) {
                    return done(err4)
                  }
                  assert.equal(0, count3)
                  db.collection(testColl4).countDocuments((err5, count4) => {
                    if (err5) {
                      return done(err5)
                    }
                    assert.equal(0, count4)
                    mongoose.connection.close((err6) => done(err6))
                  })
                })
              })
            })
          })
        })
        .catch((err) => done(err))
    })
  })

  describe('test with excluded collections to copy as promise:', () => {
    const dbDest = dbPrefix + uuid.v4()

    after(() => {
      // delete databases
      return deleteDatabase(dbDest, dbOptions)
    })

    it('copy database excluded collection promise', function (done) {
      this.timeout(20000)
      copy(dbSource, dbDest, null, [testColl2, testColl3])
        .then(function () {
          mongoose.connect(dbDest, dbOptions, (err1) => {
            if (err1) {
              return done(err1)
            }
            const db = mongoose.connection.db
            db.collection(testColl1).countDocuments((err2, count1) => {
              if (err2) {
                return done(err2)
              }
              assert.equal(4, count1)
              db.collection(testColl2).countDocuments((err3, count2) => {
                if (err3) {
                  return done(err3)
                }
                assert.equal(0, count2)
                db.collection(testColl3).countDocuments((err4, count3) => {
                  if (err4) {
                    return done(err4)
                  }
                  assert.equal(0, count3)
                  db.collection(testColl4).countDocuments((err5, count4) => {
                    if (err5) {
                      return done(err5)
                    }
                    assert.equal(4, count4)
                    mongoose.connection.close((err6) => done(err6))
                  })
                })
              })
            })
          })
        })
        .catch((err) => done(err))
    })
  })
})

function createDatabase(database, options) {
  return mongoose
    .connect(database, options)
    .then(() => {
      return Promise.all([
        mongoose.connection.db
          .collection(testColl1)
          .insertMany([{ x: 1 }, { x: 2 }, { x: 3 }, { x: 4 }], { w: 1 }),
        mongoose.connection.db
          .collection(testColl2)
          .insertMany([{ y: 1 }, { y: 2 }, { y: 3 }, { y: 4 }], { w: 1 }),
        mongoose.connection.db
          .collection(testColl3)
          .insertMany([{ z: 1 }, { z: 4 }, { z: 6 }, { z: 8 }], { w: 1 }),
        mongoose.connection.db
          .collection(testColl4)
          .insertMany([{ p: 1 }, { p: 4 }, { p: 9 }, { p: 16 }], { w: 1 }),
      ])
    })
    .then(() => {
      return mongoose.connection.close()
    })
    .catch((err) => console.error(err))
}

function deleteDatabase(database, options) {
  return mongoose
    .connect(database, options)
    .then(() => {
      return mongoose.connection.db.dropDatabase()
    })
    .then(() => {
      return mongoose.connection.close()
    })
    .catch((err) => console.error(err))
}
