'use strict'

// tslint:disable:no-console

const fs = require('fs')
const path = require('path')
const rimraf = require('rimraf')
const chai = require('chai')
const assert = chai.assert
const mongoose = require('mongoose')
const dump = require('../lib/dump.js')
const uuid = require('uuid')

const testColl1 = 'testColl1'
const testColl2 = 'testColl2'
const testColl3 = 'testColl3'
const testColl4 = 'testColl4'

describe('test dump database functions:', () => {
  const dbPrefix = 'mongodb://localhost/'
  const dbName = uuid.v4()
  const dbSource = dbPrefix + dbName
  const dbOptions = { useNewUrlParser: true }

  before(() => {
    // create database
    return createDatabase(dbSource, dbOptions)
  })

  after(() => {
    // delete databases
    return deleteDatabase(dbSource, dbOptions)
  })

  describe('test dump of whole database', () => {


    after(() => {
      // delete databases
      return deleteOutputFolder(dbName)
    })

    it('dump whole database', function(done) {
      this.timeout(20000)

      dump(['', '', '-h localhost:27017', `-d ${dbName}`, `-o ${dbName}`])
        .then(() => {
          assert.deepEqual(
            fs.readdirSync(path.resolve(__dirname, '..', dbName, dbName)),
            [
              `${testColl1}.bson`,
              `${testColl1}.metadata.json`,
              `${testColl2}.bson`,
              `${testColl2}.metadata.json`,
              `${testColl3}.bson`,
              `${testColl3}.metadata.json`,
              `${testColl4}.bson`,
              `${testColl4}.metadata.json`
            ]
          )
          done()
        })
    })
  })
})

function createDatabase(database, options) {
  return mongoose.connect(database, options)
    .then(() => {
      return Promise.all([
        mongoose.connection.db.collection(testColl1).insertMany([{ x: 1 }, { x: 2 }, { x: 3 }, { x: 4 }], { w: 1 }),
        mongoose.connection.db.collection(testColl2).insertMany([{ y: 1 }, { y: 2 }, { y: 3 }, { y: 4 }], { w: 1 }),
        mongoose.connection.db.collection(testColl3).insertMany([{ z: 1 }, { z: 4 }, { z: 6 }, { z: 8 }], { w: 1 }),
        mongoose.connection.db.collection(testColl4).insertMany([{ p: 1 }, { p: 4 }, { p: 9 }, { p: 16 }], { w: 1 })
      ])
    })
    .then(() => {
      return mongoose.connection.close()
    })
    .catch(err => console.error(err))
}

function deleteDatabase(database, options) {
  return mongoose.connect(database, options)
    .then(() => {
      return mongoose.connection.db.dropDatabase()
    })
    .then(() => {
      return mongoose.connection.close()
    })
    .catch(err => console.error(err))
}

function deleteOutputFolder(folder) {
  return new Promise((resolve, reject) => {
    rimraf(path.resolve(__dirname, '..', folder), err => {
      if (err) {
        reject(err)
      }
      resolve()
    })
  })
}
