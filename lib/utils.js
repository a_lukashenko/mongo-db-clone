// tslint:disable:no-console
'use strict'

const path = require('path')
const fs = require('fs')
const uuid = require('uuid')

function checkPrerequisites(s, d) {
  return new Promise((resolve, reject) => {
    if (s) {
      if (
        typeof s !== 'string' ||
        (!s.startsWith('mongodb://') && !s.startsWith('mongodb+srv://'))
      ) {
        return reject('source database seems to be no mongodb URL')
      }
    } else {
      return reject('source database URL is missing')
    }
    if (d) {
      if (
        typeof d !== 'string' ||
        (!d.startsWith('mongodb://') && !d.startsWith('mongodb+srv://'))
      ) {
        return reject('destination database seems to be no mongodb URL')
      }
    } else {
      return reject('destination database URL is missing')
    }
    return resolve()
  })
}

function createTmpDir() {
  return new Promise((resolve, reject) => {
    const tmpDir = uuid.v4()
    fs.mkdir(tmpDir, (err) => {
      if (err) {
        return reject('error creating tmp dir')
      } else {
        return resolve(tmpDir)
      }
    })
  })
}

function createDumpCmd(sourceDbUrl, tmpDir, includeColl, excludeColls) {
  let cmd = `${getBinDir()}/mongodump --uri ${sourceDbUrl} -o ${tmpDir}`

  if (includeColl) {
    cmd += ` -c ${includeColl}`
  } else {
    if (excludeColls) {
      if (typeof excludeColls === 'string') {
        excludeColls = [excludeColls]
      }
      for (let onedb of excludeColls) {
        cmd += ` --excludeCollection ${onedb}`
      }
    }
  }
  return cmd
}

function getDatabaseDir(tmpDir, sourceDbUrl) {
  const srcDatabaseName = sourceDbUrl.split('/').pop()
  return path.join(tmpDir, srcDatabaseName)
}

function createRestoreCmd(sourceDbUrl, destDbUrl, tmpDir) {
  const destDatabaseName = destDbUrl.split('/').pop()
  const dir = getDatabaseDir(tmpDir, sourceDbUrl)
  return `${getBinDir()}/mongorestore --uri ${destDbUrl} --drop --db ${destDatabaseName} --dir ${dir} --noIndexRestore`
}

function getBinDir() {
  const binDir = path.resolve(__dirname, '.bin')

  if (fs.existsSync(binDir)) {
    return binDir
  } else {
    console.error('bin dir not found')
  }
}

module.exports = {
  createDumpCmd: createDumpCmd,
  createRestoreCmd: createRestoreCmd,
  createTmpDir: createTmpDir,
  checkPrerequisites: checkPrerequisites,
  getDatabaseDir: getDatabaseDir,
  getBinDir: getBinDir,
}
