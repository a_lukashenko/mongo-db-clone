// tslint:disable:no-console

const path = require('path')
const fs = require('fs')
const rimraf = require('rimraf')

const { MongoDBDownload } = require('./bin-download')

// use version 4.0 because it supports all used environments
// mac 64bit - local
// ubuntu 16.04 64bit - heroku
// ubuntu 18.04 64bit - heroku
// debian 9 stretch 64bit - circleci
const version = '4.0.12'

const downloadDir = path.resolve(__dirname, 'tmp-mongo')
const binDir = path.resolve(__dirname, '.bin')

if (!fs.existsSync(path.resolve(binDir, 'mongodump'))) {
  if (!fs.existsSync(downloadDir)) {
    fs.mkdirSync(downloadDir)
  }
  if (!fs.existsSync(binDir)) {
    fs.mkdirSync(binDir)
  }

  new MongoDBDownload({ version, downloadDir })
    .downloadAndExtract()
    .then((extractLocation) => {
      const oldPath = path.resolve(extractLocation, fs.readdirSync(extractLocation)[0], 'bin')

      const bins = ['mongodump', 'mongorestore']
      bins.forEach((bin) => fs.renameSync(oldPath + '/' + bin, binDir + '/' + bin))

      rimraf(downloadDir, (err) => {
        if (err) {
          return Promise.reject(err)
        }
        return Promise.resolve()
      })
    })
    .catch((err) => console.error(err))
}
