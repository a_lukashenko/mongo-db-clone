/**
 * based on https://github.com/winfinit/mongodb-download
 *
 * The ISC License
 *
 * Copyright (c) 2018 Roman Jurkov
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted,
 * provided that the above copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* tslint:disable */

const os = require('os')
const http = require('https')
const fs = require('fs-extra')
const path = require('path')
const Debug = require('debug')
const getos = require('getos')
const url = require('url')
const semver = require('semver')
const decompress = require('decompress')
const request = require('request-promise')
const md5File = require('md5-file')

const DOWNLOAD_URI = 'https://fastdl.mongodb.org'
const MONGODB_VERSION = 'latest'

class MongoDBDownload {
  constructor({
                platform = os.platform(),
                arch = os.arch(),
                downloadDir = os.tmpdir(),
                version = MONGODB_VERSION,
                http = {},
              }) {
    this.options = {
      'platform': platform,
      'arch': arch,
      'downloadDir': downloadDir,
      'version': version,
      'http': http,
    }

    this.debug = Debug('mongodb-download-MongoDBDownload')
    this.mongoDBPlatform = new MongoDBPlatform(this.getPlatform(), this.getArch())
    this.options.downloadDir = path.resolve(this.options.downloadDir, 'mongodb-download')
    this.downloadProgress = {
      current: 0,
      length: 0,
      total: 0,
      lastStdout: '',
    }
  }

  getPlatform() {
    return this.options.platform
  }

  getArch() {
    return this.options.arch
  }

  getVersion() {
    return this.options.version
  }

  getDownloadDir() {
    return this.options.downloadDir
  }

  getDownloadLocation() {
    return new Promise((resolve, reject) => {
      this.getArchiveName().then((archiveName) => {
        let downloadDir = this.getDownloadDir()
        let fullPath = path.resolve(downloadDir, archiveName)
        this.debug(`getDownloadLocation(): ${fullPath}`)
        resolve(fullPath)
      })
    })
  }

  getExtractLocation() {
    return new Promise((resolve, reject) => {
      this.getMD5Hash().then((hash) => {
        if (!hash) {
          console.error('hash is not returned @ getExtractLocation()')
          return reject()
        }
        let downloadDir = this.getDownloadDir()
        let extractLocation = path.resolve(downloadDir, hash)
        this.debug(`getExtractLocation(): ${extractLocation}`)
        resolve(extractLocation)
      }, (e) => {
        console.error('hash is not returned @ getExtractLocation()', e)
        reject()
      })
    })
  }

  getTempDownloadLocation() {
    return new Promise((resolve, reject) => {
      this.getArchiveName().then((archiveName) => {
        let downloadDir = this.getDownloadDir()
        let fullPath = path.resolve(downloadDir, `${archiveName}.downloading`)
        this.debug(`getTempDownloadLocation(): ${fullPath}`)
        resolve(fullPath)
      })
    })
  }

  downloadAndExtract() {
    return new Promise((resolve, reject) => {
      this.download().then((archive) => {
        this.extract().then((extractLocation) => {
          resolve(extractLocation)
        })
      })
    })
  }

  extract() {
    return new Promise((resolve, reject) => {
      this.getExtractLocation().then((extractLocation) => {
        this.isExtractPresent().then((extractPresent) => {
          if (extractPresent === true) {
            resolve(extractLocation)
          } else {
            this.getDownloadLocation().then((mongoDBArchive) => {
              decompress(mongoDBArchive, extractLocation).then((files) => {
                this.debug(`extract(): ${extractLocation}`)
                resolve(extractLocation)
              }, (e) => {
                this.debug('extract() failed', extractLocation, e)
              })
            })
          }
        })
      })
    })
  }

  download() {
    return new Promise((resolve, reject) => {

      let httpOptionsPromise = this.getHttpOptions()
      let downloadLocationPromise = this.getDownloadLocation()
      let tempDownloadLocationPromise = this.getTempDownloadLocation()
      let createDownloadDirPromise = this.createDownloadDir()

      Promise.all([
        httpOptionsPromise,
        downloadLocationPromise,
        tempDownloadLocationPromise,
        createDownloadDirPromise,
      ]).then(values => {
        let httpOptions = values[0]
        let downloadLocation = values[1]
        let tempDownloadLocation = values[2]
        let downloadDirRes = values[3]

        this.isDownloadPresent().then((isDownloadPresent) => {
          if (isDownloadPresent === true) {
            this.debug(`download(): ${downloadLocation}`)
            resolve(downloadLocation)
          } else {
            this.httpDownload(httpOptions, downloadLocation, tempDownloadLocation).then((location) => {
              this.debug(`download(): ${downloadLocation}`)
              resolve(location)
            }, (e) => {
              reject(e)
            })
          }
        })
      })
    })
  }

  // TODO: needs refactoring
  isDownloadPresent() {
    return new Promise((resolve, reject) => {
      this.getDownloadLocation().then((downloadLocation) => {
        if (this.locationExists(downloadLocation) === true) {
          this.getMD5Hash().then(downloadHash => {
            md5File(downloadLocation).then(hash => {
              if (hash === downloadHash) {
                this.debug(`isDownloadPresent() md5 match: true`)
                resolve(true)
              } else {
                this.debug(`isDownloadPresent() md5 mismatch: false`)
                resolve(false)
              }
            })
          })
        } else {
          this.debug(`isDownloadPresent() location missing: false`)
          resolve(false)
        }
      })
    })
  }

  isExtractPresent() {
    return new Promise((resolve, reject) => {
      this.getMD5Hash().then(downloadHash => {
        let downloadDir = this.getDownloadDir()
        this.getExtractLocation().then((extractLocation) => {
          let present = this.locationExists(extractLocation)
          this.debug(`isExtractPresent(): ${present}`)
          resolve(present)
        })
      })
    })
  }

  getMD5HashFileLocation() {
    return new Promise((resolve, reject) => {
      this.getDownloadLocation().then((downloadLocation) => {
        let md5HashLocation = `${downloadLocation}.md5`
        this.debug(`@getMD5HashFileLocation resolving md5HashLocation: ${md5HashLocation}`)
        resolve(md5HashLocation)
      }, (e) => {
        console.error('error @ getMD5HashFileLocation', e)
        reject(e)
      })
    })
  }

  cacheMD5Hash(signature) {
    return new Promise((resolve, reject) => {
      this.getMD5HashFileLocation().then((hashFile) => {
        fs.outputFile(hashFile, signature, (err) => {
          if (err) {
            this.debug('@cacheMD5Hash unable to save signature', signature)
            reject()
          } else {
            resolve()
          }
        })
      })
    })
  }

  getMD5Hash() {
    return new Promise((resolve, reject) => {
      this.getMD5HashOffline().then((offlineSignature) => {
        this.debug(`@getMD5Hash resolving offlineSignature ${offlineSignature}`)
        resolve(offlineSignature)
      }, (e) => {
        this.getMD5HashOnline().then((onlineSignature) => {
          this.debug(`@getMD5Hash resolving onlineSignature: ${onlineSignature}`)
          resolve(onlineSignature)
        }, (e) => {
          console.error('unable to get signature content', e)
          reject(e)
        })
      })
    })
  }

  getMD5HashOnline() {
    return new Promise((resolve, reject) => {
      this.getDownloadURIMD5().then((md5URL) => {
        request(md5URL).then((signatureContent) => {
          this.debug(`getDownloadMD5Hash content: ${signatureContent}`)
          let signatureMatch = signatureContent.match(/([^\s]*)(\s*|$)/)
          let signature = signatureMatch[1]
          this.debug(`getDownloadMD5Hash extracted signature: ${signature}`)
          this.cacheMD5Hash(signature).then(() => {
            resolve(signature)
          }, (e) => {
            this.debug('@getMD5HashOnline erorr', e)
            reject()
          })
        }, (e) => {
          console.error('unable to get signature content', e)
          reject(e)
        })
      })
    })
  }

  getMD5HashOffline() {
    return new Promise((resolve, reject) => {
      this.getMD5HashFileLocation().then((hashFile) => {
        fs.readFile(hashFile, 'utf8', (err, signature) => {
          if (err) {
            this.debug('error @ getMD5HashOffline, unable to read hash content', hashFile)
            reject()
          } else {
            resolve(signature)
          }
        })
      })
    })
  }

  httpDownload(httpOptions, downloadLocation, tempDownloadLocation) {
    return new Promise((resolve, reject) => {
      let fileStream = fs.createWriteStream(tempDownloadLocation)

      let request = http.get(httpOptions, (response) => {
        this.downloadProgress.current = 0
        this.downloadProgress.length = parseInt(response.headers['content-length'], 10)
        this.downloadProgress.total = Math.round(this.downloadProgress.length / 1048576 * 10) / 10

        response.pipe(fileStream)

        fileStream.on('finish', () => {
          fileStream.close(() => {
            fs.renameSync(tempDownloadLocation, downloadLocation)
            this.debug(`renamed ${tempDownloadLocation} to ${downloadLocation}`)
            resolve(downloadLocation)
          })
        })

        response.on('data', (chunk) => {
          this.printDownloadProgress(chunk)
        })

        request.on('error', (e) => {
          this.debug('request error:', e)
          reject(e)
        })
      })
    })
  }

  getCrReturn() {
    if (this.mongoDBPlatform.getPlatform() === 'win32') {
      return '\x1b[0G'
    } else {
      return '\r'
    }
  }

  locationExists(location) {
    let exists
    try {
      let stats = fs.lstatSync(location)
      this.debug('sending file from cache', location)
      exists = true
    } catch (e) {
      if (e.code !== 'ENOENT') throw e
      exists = false
    }
    return exists
  }

  printDownloadProgress(chunk) {
    let crReturn = this.getCrReturn()
    this.downloadProgress.current += chunk.length
    let percent_complete = Math.round(
      100.0 * this.downloadProgress.current / this.downloadProgress.length * 10,
    ) / 10
    let mb_complete = Math.round(this.downloadProgress.current / 1048576 * 10) / 10
    let text_to_print =
      `Completed: ${percent_complete} % (${mb_complete}mb / ${this.downloadProgress.total}mb${crReturn}`
    if (this.downloadProgress.lastStdout !== text_to_print) {
      this.downloadProgress.lastStdout = text_to_print
      process.stdout.write(text_to_print)
    }
  }


  getHttpOptions() {
    return new Promise((resolve, reject) => {
      this.getDownloadURI().then((downloadURI) => {
        this.options.http.protocol = downloadURI.protocol
        this.options.http.hostname = downloadURI.hostname
        this.options.http.path = downloadURI.path
        this.debug('getHttpOptions', this.options.http)
        resolve(this.options.http)
      })
    })
  }

  getDownloadURI() {
    return new Promise((resolve, reject) => {
      let downloadURL = `${DOWNLOAD_URI}/${this.mongoDBPlatform.getPlatform()}`
      this.getArchiveName().then((archiveName) => {
        downloadURL += `/${archiveName}`
        let downloadURLObject = url.parse(downloadURL)
        this.debug(`getDownloadURI (url obj returned with href): ${downloadURLObject.href}`)
        resolve(downloadURLObject)
      })
    })
  }

  getDownloadURIMD5() {
    return new Promise((resolve, reject) => {
      this.getDownloadURI().then((downloadURI) => {
        let downloadURIMD5 = `${downloadURI.href}.md5`
        this.debug(`getDownloadURIMD5: ${downloadURIMD5}`)
        resolve(downloadURIMD5)
      })
    })
  }

  createDownloadDir() {
    return new Promise((resolve, reject) => {
      let dirToCreate = this.getDownloadDir()
      this.debug(`createDownloadDir(): ${dirToCreate}`)
      fs.ensureDir(dirToCreate, (err) => {
        if (err) {
          this.debug(`createDownloadDir() error: ${err}`)
          throw err
        } else {
          this.debug(`createDownloadDir(): true`)
          resolve(true)
        }
      })
    })
  }


  getArchiveName() {
    return new Promise((resolve, reject) => {
      let platform = this.mongoDBPlatform.getPlatform()
      let arch = this.mongoDBPlatform.getArch()
      let version = this.getVersion()

      switch (platform) {
        case 'osx':
          if ((version === 'latest') || semver.satisfies(version, '>=3.5')) {
            platform = `${platform}-ssl`
          }
          break
        case 'win32':
          // TODO: '2012plus' for 4.x and above
          if ((version === 'latest') || semver.satisfies(version, '>=3.5')) {
            arch = `${arch}-2008plus-ssl`
          }
          break
        default:
          break
      }
      let name = `mongodb-${platform}-${arch}`

      this.mongoDBPlatform.getOSVersionString().then(osString => {
        osString && (name += `-${osString}`)
      }, (error) => {
        // nothing to add to name ... yet
      }).then(() => {
        name += `-${this.getVersion()}.${this.mongoDBPlatform.getArchiveType()}`
        resolve(name)
      })
    })
  }
}


class MongoDBPlatform {
  constructor(platform, arch) {
    this.debug = Debug('mongodb-download-MongoDBPlatform')
    this.platform = this.translatePlatform(platform)
    this.arch = this.translateArch(arch, this.getPlatform())
  }

  getPlatform() {
    return this.platform
  }

  getArch() {
    return this.arch
  }

  getArchiveType() {
    if (this.getPlatform() === 'win32') {
      return 'zip'
    } else {
      return 'tgz'
    }
  }

  getCommonReleaseString() {
    let name = `mongodb-${this.getPlatform()}-${this.getArch()}`
    return name
  }

  getOSVersionString() {
    if (this.getPlatform() === 'linux' && this.getArch() !== 'i686') {
      return this.getLinuxOSVersionString()
    } else {
      return this.getOtherOSVersionString()
    }
  }

  getOtherOSVersionString() {
    return new Promise((resolve, reject) => {
      reject('')
    })
  }

  getLinuxOSVersionString() {
    return new Promise((resolve, reject) => {
      getos((e, os) => {
        if (/ubuntu/i.test(os.dist)) {
          resolve(this.getUbuntuVersionString(os))
        } else if (/elementary OS/i.test(os.dist)) {
          resolve(this.getElementaryOSVersionString(os))
        } else if (/suse/i.test(os.dist)) {
          resolve(this.getSuseVersionString(os))
        } else if (/rhel/i.test(os.dist) || /centos/i.test(os.dist) || /scientific/i.test(os.dist)) {
          resolve(this.getRhelVersionString(os))
        } else if (/fedora/i.test(os.dist)) {
          resolve(this.getFedoraVersionString(os))
        } else if (/debian/i.test(os.dist)) {
          resolve(this.getDebianVersionString(os))
        } else {
          // TODO: 'legacy', 'static'
          reject('')
        }
      })
    })
  }

  getDebianVersionString(os) {
    let name = 'debian'
    let release = parseInt(os.release.split('.')[0])
    if (release === 9) {
      name += '92'
    } else if (release === 8) {
      name += '81'
    } else if (release === 7) {
      name += '71'
    } else {
      this.debug('using legacy release')
    }
    return name
  }

  getFedoraVersionString(os) {
    let name = 'rhel'
    let fedora_version = parseInt(os.release)
    if (fedora_version > 18) {
      name += '70'
    } else if (fedora_version < 19 && fedora_version >= 12) {
      name += '62'
    } else if (fedora_version < 12 && fedora_version >= 6) {
      name += '55'
    } else {
      this.debug('using legacy release')
    }
    return name
  }

  getRhelVersionString(os) {
    let name = 'rhel'
    if (/^7/.test(os.release)) {
      name += '70'
    } else if (/^6/.test(os.release)) {
      name += '62'
    } else if (/^5/.test(os.release)) {
      name += '55'
    } else {
      // TODO: 'rhel57'
      this.debug('using legacy release')
    }
    return name
  }

  getElementaryOSVersionString(os) {
    let name = 'ubuntu1404'
    return name
  }

  getSuseVersionString(os) {
    let [release] = os.release.match(/(^11|^12)/) || [null]

    if (release) {
      return `suse${release}`
    } else {
      this.debug('using legacy release')
      return ''
    }
  }

  getUbuntuVersionString(os) {
    let name = 'ubuntu'
    let ubuntu_version = os.release ? os.release.split('.') : ''
    let major_version = parseInt(ubuntu_version[0])
    let minor_version = ubuntu_version[1]

    if (os.release === '12.04') {
      name += '1204'
    } else if (os.release === '14.04') {
      name += '1404'
    } else if (os.release === '14.10') {
      name += '1410-clang'
    } else if (major_version === 14) {
      // default for major 14 to 1404
      name += '1404'
    } else if (os.release === '16.04') {
      name += '1604'
    } else if (os.release === '18.04') {
      name += '1804'
    } else if (major_version === 16) {
      // default for major 16 to 1604
      name += '1604'
    } else {
      // this needs to default to legacy release, this is a BUG
      this.debug('selecting default Ubuntu release 1404')
      name += '1404'
    }
    return name
  }


  translatePlatform(platform) {
    switch (platform) {
      case 'darwin':
        return 'osx'
      case 'win32':
        return 'win32'
      case 'linux':
        return 'linux'
      case 'elementary OS': //os.platform() doesn't return linux for elementary OS.
        return 'linux'
      case 'sunos':
        return 'sunos5'
      default:
        this.debug('unsupported platform %s by MongoDB', platform)
        throw new Error(`unsupported OS ${platform}`)
    }
  }

  translateArch(arch, mongoPlatform) {
    if (arch === 'ia32') {
      if (mongoPlatform === 'linux') {
        return 'i686'
      } else if (mongoPlatform === 'win32') {
        return 'i386'
      } else {
        this.debug('unsupported mongo platform and os combination')
        throw new Error('unsupported architecture')
      }
    } else if (arch === 'x64') {
      return 'x86_64'
    } else {
      this.debug('unsupported architecture')
      throw new Error('unsupported architecture, ia32 and x64 are the only valid options')
    }
  }

}

module.exports = {
  MongoDBDownload,
  MongoDBPlatform,
}
