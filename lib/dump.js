// tslint:disable:no-console
'use strict'

const shell = require('shelljs')
const path = require('path')

/**
 * dump database
 *
 * @param args arguments (required)
 */
module.exports = (args) => {
  return Promise.resolve()
    .then(() => {
      const cmd = path.resolve(__dirname, '.bin', `mongodump ${args.slice(2).join(' ')}`)

      console.log(cmd)
      shell.exec(cmd)
      if (shell.error()) {
        return Promise.reject('Error: mongodump failed')
      }
      return Promise.resolve()
    })
}
