// tslint:disable:no-console
'use strict'

const shell = require('shelljs')
const path = require('path')

/**
 * restore database
 *
 * @param args arguments (required)
 */
module.exports = (args) => {
  return Promise.resolve()
    .then(() => {
      const cmd = path.resolve(__dirname, '.bin', `mongorestore ${args.slice(2).join(' ')}`)

      console.log(cmd)
      shell.exec(cmd)
      if (shell.error()) {
        return Promise.reject('Error: mongorestore failed')
      }
      return Promise.resolve()
    })
}
