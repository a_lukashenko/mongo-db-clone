// tslint:disable:no-console
'use strict'

const shell = require('shelljs')
const utils = require('./utils.js')

/**
 * copy one database into an emptied other.
 *
 * @param s source database url. (required)
 * @param d destination database url. (required)
 * @param i string for one included collection name. (optional)
 * @param x string of one or array of strings for excluded collection names. (optional)
 * @param callback if given returns (err), otherwise return promise.
 */
module.exports = (s, d, i, x, callback) => {
  if (typeof x === 'function') {
    // copydb(s,d,i,callback)
    callback = x
    x = null
  } else if (typeof i === 'function') {
    // copydb(s,d,callback)
    callback = i
    i = null
    x = null
  }
  let isPromise = true
  if (callback && typeof callback === 'function') {
    isPromise = false
  }
  let tmpDir

  return utils
    .checkPrerequisites(s, d)
    .then(() => {
      return utils.createTmpDir()
    })
    .then(tempDir => {
      tmpDir = tempDir
      return Promise.resolve(utils.createDumpCmd(s, tmpDir, i, x))
    })
    .then(cmd => {
      console.log(cmd)
      shell.exec(cmd)
      if (shell.error()) {
        return Promise.reject('Error: mongodump failed')
      }
      if (!shell.test('-d', utils.getDatabaseDir(tmpDir, s))) {
        return Promise.reject('Error: no data dumped')
      }
      return Promise.resolve(utils.createRestoreCmd(s, d, tmpDir))
    })
    .then(cmd => {
      console.log(cmd)
      shell.exec(cmd)
      if (shell.error()) {
        return Promise.reject('Error: mongorestore failed')
      } else {
        return Promise.resolve()
      }
    })
    .then(() => {
      shell.rm('-rf', tmpDir)
      if (isPromise) {
        return Promise.resolve()
      } else {
        callback()
      }
    })
    .catch(err => {
      if (tmpDir) {
        shell.rm('-rf', tmpDir)
      }
      if (isPromise) {
        return Promise.reject(err)
      } else {
        callback(err)
      }
    })
}
