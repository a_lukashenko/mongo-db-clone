# mongodb-copy

copy a source database into a emptied destination database.

##Installation
`npm install --save @tresmo/mongodb-copy`

##Usage as command
This package provides a `mongodb-copy` command as bin.
You must specify with `-s` with the source database url and with `-d` the destination database url.

###Example
Define a script in `package.json` to copy a database if environment variable `IS_REVIEW_APP` is set to `true`
```
"copydb": "if [ \"$IS_REVIEW_APP\" = \"true\" ]; then mongodb-copy -s $MONGO_EXPORT_SOURCE_URL -d $MONGODB_URI; fi"
```

##Usage as function
This package provides a function with the following parameter:
- The source database URL (required)
- The destination database url. (required)
- The string for one included collection name. (optional)
- The string of one or array of strings for excluded collection names. (optional)
- Callback if given returns (err), otherwise returns a promise.

###Examples
```
var mongodbCopy = require('@tresmo/mongodb-copy');
```
####Examples as Promise
- copy one database into backup: 
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup').then()...```
- copy the collection products from database one into backup: 
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup', 'products').then()...``` 
- copy all collections excluding products from database one into backup:
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup', null, 'products').then()...``` 
- copy all collections excluding products and customers from database one into backup:
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup', null, ['products','customers']).then()...``` 
  
####Examples with callback
- copy one database into backup: 
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup', functions(err) {...```
- copy the collection products from database one into backup: 
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup', 'products', functions(err) {...``` 
- copy all collections excluding products from database one into backup:
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup', null, 'products', functions(err) {...``` 
- copy all collections excluding products and customers from database one into backup:
  ```mongodbCopy( 'mongodb://localhost/one', 'mongodb://localhost/backup', null, ['products','customers'], functions(err) {...``` 

t
